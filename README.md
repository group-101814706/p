Technological specifications
Project name: Digital chart

Description

Digital chart is a platform designed to connect local businesses to customers. It allwos customers to search, discover, order and review products and services offered by businesses on the platform through a friendly usr interface.

Technological requirements

Backend:Django framework for server side development
Database:MySQl for storing user business and service data
Email API: SendGrid for order confirmations,bookings remainders and communication
Cloud Hosting:Google cloud Platform for web pplication hosting


Functional requirements

Business Account:
o Businesses can create accounts with basic information (name, contact details, location,description).
o Businesses can upload logos, photos, and videos to showcase their offerings.
o Businesses can manage their menus, service listings, or appointment slots.
o Businesses can set pricing and availability for their offerings.

Customer Functionality:
o Customers can browse businesses by category, location, or keywords.
o Customers can view detailed business profiles, menus, service descriptions, or appointment schedules.
o Customers can place orders for online delivery or pickup.
o Customers can book appointments for haircuts, massages, or fitness classes.
o Customers can leave reviews and ratings for businesses.

Admin Panel:
o Admin can manage user accounts (businesses and customers).
o Admin can monitor platform activity (orders, bookings, reviews).
o Admin can generate reports and analytics for businesses.
o Admin can manage platform settings and configurations
