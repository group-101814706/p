from django.db import models
import datetime
from django.contrib.auth.models import User
from django.conf import settings
#from .views import Product, Comment
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView

class Business(models.Model):
    name=models.CharField(max_length=50)
    contact_details=models.CharField(max_length=20)
    location=models.CharField(max_length=50)
    description=models.TextField()
    
class Category(models.Model):
    name=models.CharField(max_length=50)
    def __str__(self):
        return self.name

class Product (models.Model):
    name=models.CharField(max_length=50)
    price=models.DecimalField(default=0,decimal_places=2,max_digits=7)
    description=models.CharField(max_length=250)
    category=models.ForeignKey(Category,on_delete=models.CASCADE,default=1)
    image=models.ImageField(upload_to='uploads/Product/')
    #seller = models.ForeignKey(User, on_delete=models.CASCADE,default=1)
    def __str__(self):
        return self.name
    
class Customer(models.Model):
    fname=models.CharField(max_length=50)
    lname=models.CharField(max_length=50)
    email=models.CharField(max_length=50)
    phoneN=models.CharField(max_length=20)
    password=models.CharField(max_length=20)

    def __str__(self):
        return f"{self.fname } {self.lname}"

# class Comment(models.Model):
#        user = models.ForeignKey(User, on_delete=models.CASCADE)
#        product = models.ForeignKey(Product, on_delete=models.CASCADE)
#        text = models.TextField()
       

    
class Order(models.Model):
    product=models.ForeignKey(Product,on_delete=models.CASCADE)
    #user = models.ForeignKey(User, on_delete=models.CASCADE,default=1) 
    customer=models.ForeignKey(Customer,on_delete=models.CASCADE)
    address=models.CharField(max_length=50,blank=True)
    quantity=models.IntegerField(default=1) 
    phoneN=models.CharField(max_length=20)
    date=models.DateField(default=datetime.datetime.today)
    status=models.BooleanField(default=False)
    def __str__(self):
        return self.product




