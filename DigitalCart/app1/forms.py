from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
#from .models import Order, Business ,Comment, Customer
from django.contrib.auth.forms import UserCreationForm


class profileupdateform(UserChangeForm):
   password = None
   fname=forms.CharField(max_length=50),
   lname=forms.CharField(max_length=50),
   email=forms.CharField(max_length=50),
   phoneN=forms.CharField(max_length=20),

class SignupForm(UserCreationForm):
   class Meta:
      model = User

#class Meta:
 #       model = User
  #      fields = ('fname','lname', 'email', 'phoneN')

    
# class BusinessCreateForm(forms.ModelForm):
#     class Meta:
#         model = Business 
#         fields = ['name', 'contact_details', 'location', 'description']

# class CustomerForm(forms.ModelForm):
#     class Meta:
#         model = Customer
#         fields = ['fname', 'lname', 'email', 'phoneN','password' ]     
             
# class OrderForm(forms.ModelForm):
#     class Meta:
#         model = Order
#         fields = ['product', 'quantity', 'user']

# class CommentForm(forms.ModelForm):
#     class Meta:
#         model = Comment
#         fields = ['product', 'user', 'text']
   
