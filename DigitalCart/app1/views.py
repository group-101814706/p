from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.models import User
from django.contrib import messages 
#from .forms import OrderForm, CommentForm, BusinessCreateForm, CustomerForm
#from .forms import profileupdateform
from .models import Product, Customer
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from DigitalCart import settings
from django.core.mail import send_mail,EmailMessage
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_decode
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes, force_str
from .tokens import generate_token
#import stripe
from django.conf import settings
from django.shortcuts import render

from app2.models import Category, Item

from django.shortcuts import render
from .models import Product
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from .models import Product

def search_products(request):
    query = request.GET.get('q')
    if query:
        products = Product.objects.filter(name_icontains=query) | Product.objects.filter(seller_icontains=query)
    else:
        products = Product.objects.all()
    return render(request, 'search_results.html', {'products': products})

def index(request):
    return render(request,'index.html')
def profile_update(request):
    if request.method == "POST":
        password=request.POST['password']

    return render(request, 'profile_update.html')

def Signup(request):
    if request.method == "POST":
        username=request.POST['username']
        fname=request.POST['fname']
        lname=request.POST['lname']
        email=request.POST['email']
        password=request.POST['password']
        Cpassword=request.POST['Cpassword']

        if User.objects.filter(email=email):
            messages.error(request,"Email already registered")
            return redirect("/signup")
        if User.objects.filter(username=username):
            messages.error(request,"Username taken")
            return redirect("/signup")
        if len(username)>15:
            messages.error(request,"Username can't exceed 15 characters")
            return redirect("/signup")
        if password !=Cpassword:
            messages.error(request,"Passwords don't match")
            return redirect("/signup")
        if len(password)<8:
            messages.error(request,"Password should have more than 8 characters")
            return redirect("/signup")

        if not username.isalnum():
            messages.error(request,"Username can only contain numbers and letters")
            return redirect("/signup")   
        else:
            myuser=User.objects.create_user(username,email,password)
            myuser.first_name=fname
            myuser.last_name=lname
            myuser.is_active=False
            myuser.save()

        messages.success(request,"Account created successfuly.")
        #welcome email
        subject="Welcome to Digital Cart"
        message="Hello "+ myuser.first_name+"!\n"+"An activation link will be sent to you shortly.} "
        from_email= settings.EMAIL_HOST_USER
        to_list= [myuser.email]
        send_mail(subject,message,from_email,to_list,fail_silently=True)
        #confirmation email
        current_site= get_current_site(request)
        email_subject= "Comfirm your email @digitalcart"
        message2= render_to_string('email_comf.html',{
           'name': myuser.first_name,
           'domain': current_site.domain,
           'uid':urlsafe_base64_encode(force_bytes(myuser.pk)), 
           'token': generate_token.make_token(myuser),
        })
        email=EmailMessage(
            email_subject,
            message2,
            settings.EMAIL_HOST_USER,
            [myuser.email],
            )
        email.fail_silently= True,
        email.send()
        return redirect('/instractions')

        # messages.success(request,"Account created successfuly")
        # return redirect('/app1/signin')
    return render(request,'signup.html')

def Signin(request):
    if request.method == "POST":
        username=request.POST['username']
        password=request.POST['password']
        user=authenticate(username=username,password=password)

        if user is not None:
            login(request,user)
            # fname=user.first_name
            return render(request,'index.html')
        else:
            messages.error(request,"Incorrect password or username")
            return redirect('/signin')

    return render(request,'signin.html')
def activate(request,uidb64,token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        myuser = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        myuser = None   
    if myuser is not None and generate_token.check_token(myuser,token):
        myuser.is_active = True  
        myuser.save()
        login(request, myuser)
        return redirect('index')
    else:
        return render(request, 'activation_failed.html') 



def Signout(request):
    logout(request)
    messages.success(request,"Log out successful")
    return redirect('index')

def Des(request):
    return render(request,'description.html')
def Instractions(request):
    return render(request,'instractions.html')

def Sell(request):
    return render(request,'sell.html')
def Individualsell(request):
    return render(request,'individualsell.html')
def Categories(request):
    return render(request,'categorypage.html')
# def form(request):
#     return render(request,'form.html')    
def product_list(request):
    products = Product.objects.all()
    return render(request, 'product_list.html', {'products': products})

# def product_detail(request, pk):
#     product = Product.objects.get(pk=pk)
#     comments = Comment.objects.filter(product=product)
#     return render(request, 'product_detail.html', {'product': product, 'comments': comments}) 
  
# def add_comment(request):
#     if request.method == 'POST':
#         comment_form = CommentForm(request.POST)
#         if comment_form.is_valid():
#             comment_form.save()
#             return redirect('index')
#     return redirect('index')

# def make_order(request):
#     if request.method == 'POST':
#         order_form = OrderForm(request.POST)
#         if order_form.is_valid():
#             order_form.save()
#             return redirect('/app1/index')
#     return redirect('/app1/index')


    
class ProductCreateView(CreateView):
    model = Product
    fields = ['name', 'price', 'description', 'category', 'image', 'seller']
    template_name = 'products/product_form.html'  
    success_url = '/product_list/'      

    
class ProductDetailView(DetailView):
    model = Product
    template_name = 'product_detail.html'  
    context_object_name = 'product' 


# def create_business(request):
#     if request.method == 'POST':
#         form = BusinessCreateForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('business_profile')  
#     else:
#         form = BusinessCreateForm()
#     return render(request, 'create_business.html', {'form': form})

# def Customer(request):
#     if request.method == 'POST':
#         form = CustomerForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('customer_profile')


# 
def index(request):
    items=Item.objects.all()
    Categories=Category.objects.all()
    return render(request, 'index.html', {
        'items':items,
        'categories':Categories
    })
def about(request):
    return render(request, 'about.html')

def contact(request):
    return render(request, 'contact.html')

def search(request):
    return render(request, 'search.html', {})

def add_to_cart(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    
    # Assuming you're using sessions to store cart items
    cart = request.session.get('cart', {})
    cart[product_id] = cart.get(product_id, 0) + 1
    request.session['cart'] = cart
    
    return redirect('product-detail', pk=product_id)


def cart_view(request):
    cart = request.session.get('cart', {})
    cart_items = []
    
    for product_id, quantity in cart.items():
        product = get_object_or_404(Product, pk=product_id)
        cart_items.append({
            'product': product,
            'quantity': quantity,
        })
    
    return render(request, 'cart.html', {'cart_items': cart_items})
