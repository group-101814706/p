from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_views
from .views import search_products




urlpatterns=[
    path("", views.index, name='index'),
    path("about/", views.about, name='about'),
    path('contact/', views.contact, name='contact'),
    path('search/', views.search, name='search'),
    path('signin/',views.Signin),
    path('signup/',views.Signup),
    path('signout/',views.Signout),
    path('description/',views.Des),
    path('instractions/',views.Instractions),
    path('activate/<uidb64>/<token>/', views.activate, name='activate'),
    path('Password_reset/', auth_views.PasswordResetView.as_view(template_name='password_reset_form.html'), name='Password_reset'),
    path('password_reset_done/', auth_views.PasswordResetDoneView.as_view(template_name='password_reset_done.html'), name='password_reset_done'),
    path('password_reset_confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='password_reset_confirm.html'), name='password_reset_confirm'),
    path('password_reset_complete/', auth_views.PasswordResetCompleteView.as_view(template_name='password_reset_complete.html'), name='password_reset_complete'),
    path('individualsell',views.Individualsell),
    path('sell',views.Sell),
    path('category',views.Categories),
    # path('<int:pk>/', views.product_detail, name='product_detail'),
    # path('make_order/', views.make_order, name='make_order'),
    # path('add_comment/', views.add_comment, name='add_comment'),
    path('product_list/', views.product_list, name='product_list'),
    path('Products/<int:pk>/', views.ProductDetailView.as_view(), name='product'),
    # path('login/',views.Login_register,name='login'),
    # path('sell/',views.Sell,name='sell'),
    path('profile_update/', views.profile_update, name='profile_update.html'),
    path('about/', views.about, name='about'),
    path('search/',views.search, name='search'),
    path('contact_us/',views.contact),
    
]