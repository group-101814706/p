from django.shortcuts import render, get_object_or_404
from django.db.models import Q
from .models import Item
from app2.models import Category, Item
from .forms import RegisterForm


def search(request):
     query = request.GET.get('quert', '')
     Items = Item.objects.filter(Q(title_icontains=query) | Q(description_icontains=query))


     return render(request, 'products/search.html', {
         'query':query,
         'items': Items,
     })


def detail(request, pk):
    item = get_object_or_404(Item, pk=pk)
    related_items = Item.objects.filter(category=item.category).exclude(pk=pk)

    return render(request, 'products/detail.html',{
        'item':item,
        'related_items': related_items
    })
    
def register_business(request):
    if request.method == 'GET':
        form = RegisterForm()
        return render(request, 'register_business.html', {'form': form})

def category(request, pk):
    category = get_object_or_404(Category, pk=pk)
    items = Item.objects.filter(category=category)
    
    return render(request, 'categorypage.html', {
        'category': category,
        'items': items,
        })
