from django.db import models
from django.contrib.auth.models import User

class Category(models.Model):
    name = models.CharField(max_length=150)

    class Meta:
        ordering = ('name',)
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name
    
class Item(models.Model):
    user = models.ForeignKey(User, related_name='items', on_delete=models.CASCADE)
    category = models.ForeignKey(Category, related_name='items', on_delete=models.CASCADE)
    name = models.CharField(max_length=150)
    description = models.CharField(max_length=200, blank=True)
    details = models.TextField(blank=True, null=True)
    globalprice = models.CharField(max_length=15)
    sellingprice = models.CharField(max_length=15)
    image = models.ImageField(upload_to='vendor_products')
    In_stock = models.BooleanField(default=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    class Meta:
        ordering = ('-created_at',)


    def discounted_price(self):
        discounted_price = int(((int(self.globalprice)-int(self.sellingprice))/int(self.globalprice))*100)
        return discounted_price
    
    def __str__(self):
        return self.name


class Info(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    businame = models.CharField(max_length=150)
    about = models.TextField(max_length=200)
    slogan = models.CharField(max_length=150)
    services_offered = models.TextField(max_length=500)
    location = models.CharField(max_length=200)
    email1 = models.EmailField(max_length=100)
    email2 = models.EmailField(max_length=100)
    phone1 = models.CharField(max_length=10)
    phone2 = models.CharField(max_length=100)
    other_branches = models.CharField(max_length=200)
    logo = models.ImageField(upload_to='vendor_logos/', default='default_logo.jpg')