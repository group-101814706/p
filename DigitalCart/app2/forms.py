from django import forms
from .models import Item,Info
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class ItemForm(forms.ModelForm):
    class Meta:
        model= Item
        fields= ('category', 'name', 'description', 'details', 'globalprice', 'sellingprice', 'image',)
        


class InfoForm(forms.ModelForm):
    class Meta:
        model = Info
        fields = ['businame', 'about', 'slogan', 'services_offered', 'location', 'email1', 'email2', 'phone1', 'phone2', 'other_branches', 'logo']



class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1']
