from django.urls import path

from . import views


app_name = 'app2'

urlpatterns = [
    # path('search/', views.search, name='search'),
    path('<int:pk>/', views.detail, name='detail'),
    path('register/', views.register_business,name='register_business'),
    path('categories/<int:pk>/', views.category, name='categorypage'),
]