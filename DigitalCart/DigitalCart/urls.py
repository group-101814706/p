
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path



urlpatterns = [
    path("", include('app1.urls')),
    path('admin/', admin.site.urls),
    path('items/', include('app2.urls')),
    path('', include('app3.urls') ),
   

]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)