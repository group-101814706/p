from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from app2.models import Item, Info
from django.contrib import messages
from app2.forms import ItemForm, InfoForm
from .models import Userprofile
#from .forms import RegisterForm




def vendor_detail(request, pk):
    user = User.objects.get(pk=pk)

    return render(request, 'userprofile/vendor_detail.html',{
        'user': user
    })


def info_list(request):
    infos = Info.objects.all()
    return render(request, 'userprofile/vendor_detail.html', {
        'infos': infos
    })

def info_detail(request, info_id):
    info = get_object_or_404(Info, id=info_id)
    return render(request, 'userprofile/vendor_detail.html', {'info': info})




@login_required
def create_info(request):
    if request.method == 'POST':
        form = InfoForm(request.POST)
        if form.is_valid():
            info = form.save(commit=False)
            info.user = request.user
            info.save()
            return redirect('vendor_detail')
    else:
        form = InfoForm()
    return render(request, 'userprofile/info.html', {'form': form})

@login_required
def my_shop(request):
    return render(request, 'userprofile/my_shop.html')

@login_required
def add_product(request):
    if request.method == 'POST':
        form = ItemForm(request.POST, request.FILES)

        if form.is_valid():
            item = form.save(commit=False)
            item.user = request.user
            item.save()

            messages.success(request, 'Product added Successfully')
            
            return redirect('my_shop')
    else:
        form = ItemForm()

    return render(request, 'userprofile/add_product.html', {
        'title': 'Add product',
        'form': form
    })

def edit_product(request, pk):
    item = get_object_or_404(Item, pk=pk, user=request.user)

    if request.method == 'POST':
        form = ItemForm(request.POST, request.FILES, instance=item)
        if form.is_valid():
            form.save()
            messages.success(request, 'Product Changes Saved')
            return redirect('my_shop')
    else:
        form = ItemForm(instance=item)

    return render(request, 'userprofile/edit_product.html', {
        'form': form,
        'item': item
    })


@login_required 
def delete_item(request, pk):
    item = get_object_or_404(Item, pk=pk, user=request.user)
    if request.method == 'POST':
        item.delete()
        messages.success(request, 'Item deleted successfully.')
    return redirect('my_shop')


@login_required 
def myaccount(request):
    return render(request, 'userprofile/myaccount.html')

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            user = form.save()

            login(request, user)

            Userprofile = Userprofile.objects.create(user=user)

            return redirect('index')     
    else:
        form = UserCreationForm()

    return render(request, 'userprofile/signup.html',{
            'form': form
    })  
    