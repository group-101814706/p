from django.urls import path 
from django.contrib.auth import views as auth_views
from . import views


urlpatterns = [
   # path('signup/', views.signup, name='singup'),
    # path('login/', auth_views.LoginView.as_view(template_name='userprofile/login.html'), name='login'),
    # path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('myaccount/', views.myaccount, name='myaccount'),
    path('myshop/', views.my_shop, name='my_shop'),
    path('myshop/add_product', views.add_product, name='add_product'),
    path('myshop/edit-product/<int:pk>/', views.edit_product, name='edit_product'),
    path('vendors/<int:pk>/', views.vendor_detail, name='vendor_detail'),
    path('info/', views.info_list, name='vendor_detail'),
    path('my_shop/info', views.create_info, name='info'),
]   